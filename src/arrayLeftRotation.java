/*
    Author: George Marshall
    Date: 12/10/19

    A left rotation operation on an array shifts each of the array's elements 1 unit to the left. For example, if 2 left rotations are performed on array 1,2,3,4,5, then the array would become 3,4,5,1,2.
    Given an array a of n integers and a number, d, perform d left rotations on the array. Return the updated array to be printed as a single line of space-separated integers.

    Function Description

    Complete the function rotLeft in the editor below. It should return the resulting array of integers.
    rotLeft has the following parameter(s):
    An array of integers a .
    An integer d, the number of rotations.
*/



import java.io.*;
        import java.math.*;
        import java.security.*;
        import java.text.*;
        import java.util.*;
        import java.util.concurrent.*;
        import java.util.regex.*;

public class arrayLeftRotation {

    // Complete the rotLeft function below.
    static int[] rotLeft(int[] a, int d) {
        for(int i = 0; i<d; i++){   //goes through the array d times
            int temp = a[0];;
            for(int j = 0; j< a.length-1; j++){     //shuffles the array one position to the left
                a[j] = a[j+1];

            }
            a[a.length-1]=temp; //puts the original position one into the last position.
        }
        return a;


    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
       Scanner scan = new Scanner(System.in);
       System.out.println("Please enter the array size:");
       int len = scan.nextInt();
        System.out.println("Please enter the array:");
        int[] cleanArray = new int[len];
       for(int i = 0; i< len; i++){
           cleanArray[i] = scan.nextInt();

       }
        System.out.println("Please enter the amount you would like to shuffle the array:");
       int shuffle = scan.nextInt();
        scanner.close();




        System.out.println(Arrays.toString(rotLeft(cleanArray, shuffle)));
    }
}
