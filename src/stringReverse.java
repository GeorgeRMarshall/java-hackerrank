/*
    Author: George Marshall
    Date: 15/10/19

    Given a string A, print Yes/True if it is a palindrome, print No/ False otherwise.
 */

import java.util.*;
public class stringReverse {
    public static void main(String args[]){

        Scanner scan = new Scanner (System.in);
        String input = scan.next();
        boolean palindrome = true;
        for(int i=0;i<(input.length()/2);i++){
            if(input.charAt(i)!=input.charAt((input.length()-1)-i)){
                palindrome = false;
                break;
            }
        }
        System.out.println(input + " is a palindrome? " + palindrome);
    }

}
