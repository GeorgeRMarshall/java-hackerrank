/*
    Author: George Marshall
    Date: 15/10/19

    Write the following code:

    Create an array, arr[] a0, a1,.... capable of holding I integers.
    Modify the code in the loop so that it saves each sequential value to its corresponding location in the array. For example, the first value must be stored in a0, the second value must be stored in a2, and so on.
 */

import java.util.*;
public class onedArray {
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        int arrSize = scan.nextInt();
        int[] integerArray = new int[arrSize];
        for(int i = 0; i<arrSize; i++){
            integerArray[i]=scan.nextInt();

        }
        scan.close();
        System.out.println(Arrays.toString(integerArray));


    }
}
