Here are my completed solutions to the Java questions from HackerRank along with an index (Question number and title)
Each solution contains the description from HackerRank about the question and depending on the question input and output. All answers are located in the src folder<br /><br />
My HackerRank profile is available at https://www.hackerrank.com/georgermarshall<br /> <br />
Q1 - Print Hello World *Please note I did not upload this to Gitlab*   <br />
Q2 - Java Stdin and Stdout I *Please note I did not upload this to Gitlab* <br />
Q3 - Java Stdin and Stdout II - Named StdinandStdout2.java <br />
Q4 - Java Output Formatting - Named  OutputFormatting.java <br />
Q5 - Java Loops I - Named Loops1.java <br />
Q6 - Java Loops II - Named Loops2.java <br />
Q7 - Java Datatypes - Names Datatypes.java <br />
   
